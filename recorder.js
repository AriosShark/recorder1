let video = document.querySelector("#video");
document.querySelector("#boton").addEventListener("click",function(ev){
	navigator.mediaDevices.getUserMedia({audio: true, video:true})
	.then(record)
	.catch(err => console.log(err));
})

let chunks =[];

function record(stream){
video.srcObject = stream;

let mediaRecorder = new MediaRecorder(stream,{
	nimeType: 'video/mp4;codecs=h264'
});
 mediaRecorder.start();

 mediaRecorder.ondataavailable = function(e){
	 console.log(e.data);
	 chunks.push(e.data);
	
 }

 mediaRecorder.onstop = function(){

	alert("finalizo la grabacion");

	let blob = new Blob(chunks,{type:"video/mp4"});

	chunks = [];

	download(blob)

 }
 setTimeout(()=> mediaRecorder.stop(),10000);
}

function download(blob){
	let link = document.createElement("a");
	link.href = window.URL.createObjectURL(blob);
	link.setAttribute("download","video_recorded");
	link.style.display = "none";

	document.body.appendChild(link);
	link.click();
	link.remove();
}